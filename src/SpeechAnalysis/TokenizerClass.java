package SpeechAnalysis;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.PTBTokenizer;

public class TokenizerClass {
	
	static String datapath = "E:\\workspace\\SpeechAnalysis\\data\\";

	Set<String> namoLabels = null;
	Set<String> rgLabels = null;

	int stopcount;
	int gram; 
	
	static int sentenceFileCount = 0;
	
	TreeMap<String, Integer> totalDict = null;

	HashMap<String, Integer> namoDict = null;
	HashMap<String, Integer> rgDict = null;

	public TokenizerClass(int stopcount, int gram) {
		totalDict = new TreeMap<String, Integer>();
		namoDict = new HashMap<String, Integer>();
		rgDict = new HashMap<String, Integer>();
		this.stopcount = stopcount;
		this.gram = gram;
	}

	/***
	 * Get tokens and produce token list
	 */
	public void GetTokensAndProduceTokenList() {

		namoLabels = new HashSet<String>();
		rgLabels = new HashSet<String>();

		File namoFolder = new File(datapath+"namo");
		File rgFolder = new File(datapath+"rg");

		File[] namoListOfFiles = namoFolder.listFiles();
		File[] rgListOfFiles = rgFolder.listFiles();

		for (File file : namoListOfFiles) {
			if (file.canRead()) {
				try {
					namoLabels.addAll(this.GetTokens(file.getAbsolutePath(), "namo", true));
					this.GetSentences(file.getAbsolutePath(), "namo");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		for (File file : rgListOfFiles) {
			if (file.canRead()) {
				try {
					rgLabels.addAll(this.GetTokens(file.getAbsolutePath(), "rg", true));
					this.GetSentences(file.getAbsolutePath(), "rg");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		// Get Stop words
		List<String> stopWords = GetStopWords(totalDict);
		String path = datapath+"processeddata\\stop.list";
		WriteListToFile(path, stopWords);

		// remove stop words
		for (String s : stopWords) {
			totalDict.remove(s);
			namoDict.remove(s);
			rgDict.remove(s);
		}
		
		System.out.println("Size : " + totalDict.size());
		List<String> masterKeySet = new ArrayList<String>(totalDict.keySet());				
		
		// generate speaker level dict file
		WriteDictionaryToFile(totalDict, "totalDict");
		WriteDictionaryToFile(namoDict, "namo");
		WriteDictionaryToFile(rgDict, "rg");		
		
		File namoSentenceFolder = new File(datapath+"namosentences");
		File rgSentenceFolder = new File(datapath+"rgsentences");

		File[] namoSentenceListOfFiles = namoSentenceFolder.listFiles();
		File[] rgSentenceListOfFiles = rgSentenceFolder.listFiles();
		
		// Create two sentence level features 
		CreateSVMFormatFile(masterKeySet, namoSentenceListOfFiles, stopWords,  "namo");
		CreateSVMFormatFile(masterKeySet, rgSentenceListOfFiles, stopWords, "rg");
	}
	
	/***
	 * create svm files
	 * @param s1
	 * @param s2
	 */
	public void CreateSVMFormatFile(List<String> masterSet, File[] files, List<String> stopList, String speaker) {

		File fullSVMData = new File(datapath+"processeddata\\total.svm");
		File train = new File(datapath+"processeddata\\train.svm");
		File test = new File(datapath+"processeddata\\test.svm");
		
		Integer svmclass = 1;
		
		if (speaker.equalsIgnoreCase("rg")){
			svmclass = -1;
		}
		
		List<Integer> randoms = new ArrayList<Integer>();
		
		// generate random numbers for test
		if (speaker.equals("namo")){
			for (int i = 0; i < 60; i++){
				Double a = 0.0;
				while(((a = Math.random()* (538.0)) > 0) && !randoms.contains(a.intValue())){
					randoms.add(a.intValue());
					break;
				}
			}
		}
		else if (speaker.contains("rg")){
			for (int i = 0; i < 20; i++){
				Double a = 0.0;
				while(((a = Math.random()* (188.0)) > 0) && !randoms.contains(a.intValue())){
					randoms.add(a.intValue());
					break;
				}
			}
		}
		
		int k = 0;
		
		try {
			
			FileWriter fw = new FileWriter(fullSVMData.getAbsoluteFile(), true);
			BufferedWriter bw = new BufferedWriter(fw);
			FileWriter fwtr = new FileWriter(train.getAbsoluteFile(), true);
			BufferedWriter bwtr = new BufferedWriter(fwtr);
			FileWriter fwts = new FileWriter(test.getAbsoluteFile(), true);
			BufferedWriter bwts = new BufferedWriter(fwts);
						
			for (File file : files) {
				if (file.canRead()) {
					List<String> tokens = this.GetTokens(file.getAbsolutePath(), "somespeaker", false);
					
					// remove stop words
					tokens.removeAll(stopList);
					
					if (tokens.size() > 0){
						TreeMap<String, Integer> dict = this.GetMapFromList(tokens);
						
						StringBuilder svmStr = new StringBuilder(svmclass.toString() + " ");
						for (String s : dict.keySet()){
							svmStr.append(masterSet.indexOf(s)+1 + ":" + dict.get(s) + " ");						
						}
						svmStr.append("\n");
						
						bw.append(svmStr);
						
						if (randoms.contains(k)){
							bwts.append(svmStr);
						}
						else{
							bwtr.append(svmStr);
						}
					}
				}	
				
				k++;
			}
			
			bw.close();
			bwtr.close();
			bwts.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/***
	 * 
	 * @param list
	 * @return
	 */
	public TreeMap<String, Integer> GetMapFromList(List<String> list){
		
		TreeMap<String, Integer> dict = new TreeMap<String, Integer>();
		
		for(String s : list){
			if (dict.containsKey(s)) {
				dict.put(s, dict.get(s) + 1);
			} else {
				dict.put(s, 1);
			}
		}
		
		return dict;
	}
	
	/***
	 * makes sentence out of two lines
	 * @param filepath
	 * @return
	 */
	public List<String> GetSentences(String filepath, String speaker){
		
		List<String> list = new ArrayList<String>();
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(filepath));
			
			String s = "";
			while((s = br.readLine()) != null){
				list.add(s);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}		
		
		List<String> list1 = new ArrayList<String>();
		
		int i = 0;
		
		for (; i + 1< list.size(); i += 2){
			list1.add(list.get(i) + " " + list.get(i+1));
		}
		
		// if number of lines is not multiple of two, add the last line 
		if (list.size() % 2 != 0){
			list1.add(list.get(list.size()-1));
		}
		
		for (i = 0; i < list1.size(); i++){
			this.WriteStringToFile(datapath+speaker+"sentences\\"+sentenceFileCount+".txt", list1.get(i));
			sentenceFileCount++;		
		}
		
		return list1;
	}
	
	/***
	 * write the given string to file
	 * @param path
	 * @param str
	 */
	public void WriteStringToFile(String path, String str){
		File f = new File(path);
		FileWriter fw;
		try {
			fw = new FileWriter(f.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(str);
			
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}
	
	/***
	 * write the given list to file
	 * @param path
	 * @param list
	 */
	public void WriteListToFile(String path, List<String> list){
		File f = new File(path);
		FileWriter fw;
		try {
			fw = new FileWriter(f.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			
			for (int i = 0; i < list.size(); i++){
				bw.append(list.get(i) + "\n");
			}
			
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}
		
	/***
	 * Get Tokens
	 * @param str
	 */
	public List<String> GetTokens(String path, String speaker, boolean updateTotalDict) {

		PTBTokenizer ptbt = null;

		List<String> listOfLabels = new ArrayList<String>();
		List<String> returnStr = new ArrayList<String>();
		TreeMap<String, Integer> dict = new TreeMap<String, Integer>();

		try {
			ptbt = new PTBTokenizer(new FileReader(path), new CoreLabelTokenFactory(), "");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		for (CoreLabel label; ptbt.hasNext();) {
			label = (CoreLabel) ptbt.next();
			listOfLabels.add(label.toString());
		}
				
		// generate according to n-gram
		for (int i = 0; i < listOfLabels.size() / this.gram; i++) {
			int j = 0;
			String s = "";
			while (j < this.gram) {
				s += " " + listOfLabels.get(i * this.gram + j);
				j++;
			}
			String trimmedS = s.trim();
			returnStr.add(trimmedS);
			
			if (updateTotalDict){
				
				if (dict.containsKey(trimmedS)) {
					dict.put(trimmedS, dict.get(trimmedS) + 1);
				} else {
					dict.put(trimmedS, 1);
				}
				
				if (speaker.equalsIgnoreCase("namo")) {
					namoDict.putAll(dict);
				} else if (speaker.equalsIgnoreCase("rg")) {
					rgDict.putAll(dict);
				}
				
				if (totalDict.containsKey(trimmedS)) {
					totalDict.put(trimmedS, totalDict.get(trimmedS) + 1);
				} else {
					totalDict.put(trimmedS, 1);
				}
			}				
		}
		
		return returnStr;
	}
	
	/***
	 * write dictionary to file
	 * @param dict
	 * @param speaker
	 */
	public void WriteDictionaryToFile(Map<String,Integer> dict, String speaker) {
		// write the dict to files
		
		try {
			File f = new File(datapath + "processeddata\\"+ speaker + ".dict");
			FileWriter fw = new FileWriter(f.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);

			for (String s : dict.keySet()) {
				String s1 = s + " : " + dict.get(s) + "\n";
				bw.write(s1);
			}

			bw.close();
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/***
	 * remove the stop words
	 * 
	 * @param tmap
	 * @return
	 */
	private List<String> GetStopWords(TreeMap<String, Integer> tmap) {

		// sort dict based on collection frequency
		Set<Entry<String, Integer>> set = tmap.entrySet();
		List<Entry<String, Integer>> list = new ArrayList<Entry<String, Integer>>(
				set);
		Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
			public int compare(Map.Entry<String, Integer> o1,
					Map.Entry<String, Integer> o2) {
				return (o2.getValue()).compareTo(o1.getValue());
			}
		});

		List<String> stoplist = new ArrayList<String>();

		// remove stop words, stopcount most frequent grams
		for (int i = 0; i < this.stopcount; i++) {
			System.out.println("removed : " + list.get(i).getKey() + " : "
					+ list.get(i).getValue());

			stoplist.add(list.get(i).getKey());
		}

		return stoplist;
	}

}
