Dr. Dalia Schipper, Mr. S Mahalingam, My Colleague Mr. Vaju bhai Vala, Mr. Joti, Ladies and Gentlemen!
We have assembled here to discuss a very important issue � empowering our youth with the Knowledge and Skill-sets of the 21st century. For me, this in fact is the most important dimension of our great nation�s developmental story. I am extremely happy to be here.
More than 50% of India�s population is below 25 years of age. We enjoy an average age of 24 years � while a large part of the world stares at an ageing population.
I see India�s youth as one of its biggest strengths and most valuable resources. I strongly believe that if India is able to invigorate its talent pool � its youth in one decade will be the engine of growth not just for India but for the world. If we convert our youth population into a skilled workforce, we can meet the entire world�s workforce requirements.

In line with this strong belief in Youth Power � Gujarat has invested substantial time and resource in facilitating their all round development. We believe education and skill development are the most powerful mechanisms for empowering our youth.
With a holistic approach we are increasing the reach of education and training. More importantly, we are also enhancing its quality. This is being done in line with the latest international trends in promoting innovation and nurturing human resource.
And the results are visible for all to see. Gujarat presents the best picture in employment generation. A recent survey of Government of India says that Gujarat has the least unemployment rate.
Gujarat also presents the best picture in development of skills � hard as well as soft.
Many of our initiatives are now recognized as best practices nationally. To give a few leading examples:
Our iCREATE initiative is being hailed by everyone for its innovativeness.
Through SCOPE we are building English language proficiency in the youth of Gujarat.
Recently, we have launched eMPOWER to prepare our youth in IT and electronics skill-sets.
Various other initiatives nurturing all-round development include the likes of Vaanche Gujarat � a drive for reading, Khel Mahakumbh � a statewide drive for encouraging various sports, Innovation Commission, Choice Based Credit System and so on
 
Specific to the domain of skill development � Gujarat has given top priority to developing modern technical and professional skills in our youth
The Gujarat Skill Development Mission & Gujarat Council of Vocational Training aim to make each and every youth employable. Our Industrial Training Institutes (ITIs) play a central role in this. The number of ITIs have increased five-fold over the last decade. Our ITIs have been upgraded with new buildings, new machinery and state-of-the art infrastructure.
Courses have been updated, and their number and diversity dramatically expanded.
20 Superior Technology Centres (STCs) have been launched � which provide specialized training using state of the art technology. These are driven by industry needs and future human resource demand.  Examples would include STCs related to Automobile Servicing and Solar Technology. We have also opened a powerful window of opportunity by enabling ITI students to pursue Diploma and Engineering courses after their ITI education, thus widening their career horizons.
One of the best parts of the story is that our efforts are powered through partnership with the industry. More than 50% of our ITIs are now running on PPP basis � through partnerships with a range of national and international companies and institutions. This is going to be further strengthened today with MoUs being signed for setting up new Skill Generation Centres
My experiences in Gujarat reaffirm my strong belief that India must place at the top of its agenda the task of empowering its youth with Knowledge and Skill-sets. We in Gujarat are already sowing the seeds for the same.
We remain committed to turning every young mind of Gujarat into a powerhouse of motivation, hard work and innovation.
From this perspective, this convention is very important. That is why we have organized it as an important part of our 6th Vibrant Gujarat Global Summit. I look forward to the deliberations throwing up fresh perspectives and new ideas on how we can do even better �
 
- Narendra Modi