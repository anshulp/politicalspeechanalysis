Salaam vaalekum. 
Brothers and Sisters, very welcome to you and namashkaar. 

I am very happy to visit this beatiful place. I have a long traditional link to this place. Nehru, Indira and Rajiv, all used to come to this place and they had a very big place in their hearts for Kargil, leh, Ladakh. When I had come here last time, I had told you that I would look after your affairs in Delhi, I will fight your battle in Delhi, your voices don't reach Delhi so I would carry your voice to Delhi. And you had asked me for three things of which first one was to connect Zojila Tunnel Ladakh to whole of India for full year. We have taken that first step. Zojila tunnel is just the first step, after that the second step of the tunnel would be done as soon as possible. I have talked to the government regarding this, I have talked to Planning commission and they have assured me of starting the big tunnel also as soon as possible. 
Second thing that you had told me was that there was no air connectivity here. When somebody is sick there is a problem. Air planes cannot land here, so I have talked to Civil aviation minister and he has assured me that air connectivity would start in three months. 
And third thing was about the telephone connectivity. You had said that there is lack of BSNL connectivity. We have started working in this area also. Equipment has arrived but is stuck due to a court case, but I would talk to Jitendra singh and we are trying to work out other solution so that telephone connectivity reaches here as early as possible. 
And I had talked to you earlier also that I want to create a relationship with you, a long relationship, a life long relationship. Howsoever you need me, I am always ready. When we were attached here, you were standing, fighting. When there was a need you fought also; you fight for the nation. 
Your voices would reach delhi, Sonia would carry your voices, I would carry them, prime minister would carry them. No matter you are far from Delhi, you are always iYourn our hearts. Your voices would definitely echo, and whatever we can do we would do and would try to do that the whole life. 
You have come from far far places. Thank you.
Jai Hind




